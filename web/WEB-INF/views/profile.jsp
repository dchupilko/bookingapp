<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css" />">
</head>
<body>
<div class="personView">
    <h1>Your profile</h1>
    <c:out value="${person.username}"/><br/>
    <c:out value="${person.firstName}"/> <c:out value="${person.lastName}"/>
</div>
</body>
</html>
