<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css" />"
    >
</head>
<body>
    <h1>Welcome to Booking App!</h1>

    <a href="<c:url value="/resources" />">Resources</a>
    <a href="<c:url value="/register" />">Register</a>
</body>
</html>
