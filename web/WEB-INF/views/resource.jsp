<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css" />">
</head>
<body>
<div class="resourceView">
    <div class="resourceName"><c:out value="${resource.name}"/></div>
    <div>
        <span class="resourceOccupancy"><c:out value="${resource.occupancy}"/></span>
    </div>
</div>
</body>
</html>
