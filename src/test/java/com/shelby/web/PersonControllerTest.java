package com.shelby.web;

import com.shelby.data.domain.Person;
import com.shelby.data.service.BookingService;
import org.hibernate.validator.HibernateValidator;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class PersonControllerTest {

    @Test
    public void showRegistrationForm() throws Exception {
        BookingService repositoryMock = mock(BookingService.class);

        PersonController controller = new PersonController(repositoryMock);
        MockMvc mockMvc = standaloneSetup(controller).setViewResolvers(getViewResolver()).build();

        mockMvc.perform(get("/profile/new"))
                .andExpect(view().name("register"));
    }

    @Test
    public void processRegistration() throws Exception {
        BookingService repositoryMock = mock(BookingService.class);
        Person unsaved = new Person("jsmith", "12345", "John", "Smith");
        Person saved = new Person(1L, "jsmith", "12345", "John", "Smith");
        when(repositoryMock.savePerson(unsaved)).thenReturn(saved);

        PersonController controller = new PersonController(repositoryMock);
        MockMvc mockMvc = standaloneSetup(controller).setViewResolvers(getViewResolver()).build();

        mockMvc.perform(post("/profile/new")
                .param("username", "jsmith")
                .param("password", "12345")
                .param("firstName", "John")
                .param("lastName", "Smith"))
                .andExpect(redirectedUrl("/profile/jsmith"));
    }

    @Test
    public void processRegistration_shouldFail() {
        LocalValidatorFactoryBean localValidatorFactory = new LocalValidatorFactoryBean();
        localValidatorFactory.setProviderClass(HibernateValidator.class);
        localValidatorFactory.afterPropertiesSet();

        Person unsaved = new Person("A", "B", "C", "D");
        Set<ConstraintViolation<Person>> constraintViolations = localValidatorFactory.validate(unsaved);

        Assert.assertEquals("Expected validation error not found", 4, constraintViolations.size());
    }

    @Test
    public void shouldShowPersonById() throws Exception {
        Person expectedPerson = new Person("jsmith", "12345", "John", "Smith");
        BookingService repositoryMock = mock(BookingService.class);
        when(repositoryMock.findPersonByUsername("jsmith")).thenReturn(expectedPerson);

        PersonController controller = new PersonController(repositoryMock);
        MockMvc mockMvc = standaloneSetup(controller).setViewResolvers(getViewResolver()).build();

        mockMvc.perform(get("/profile/jsmith"))
                .andExpect(view().name("profile"))
                .andExpect(model().attributeExists("person"))
                .andExpect(model().attribute("person", expectedPerson));
    }

    @Test
    public void shouldShowPersonById_personNotFound() throws Exception {
        BookingService repositoryMock = mock(BookingService.class);
        when(repositoryMock.findPersonByUsername("jsmith")).thenReturn(null);

        PersonController controller = new PersonController(repositoryMock);
        MockMvc mockMvc = standaloneSetup(controller).setViewResolvers(getViewResolver()).build();

        mockMvc.perform(get("/profile/jsmith"))
                .andExpect(status().isNotFound());
    }

    private ViewResolver getViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/templates/");
        resolver.setSuffix(".html");
        resolver.setExposeContextBeansAsAttributes(true);
        return resolver;
    }
}
