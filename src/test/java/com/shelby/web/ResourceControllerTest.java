package com.shelby.web;

import com.shelby.data.domain.Resource;
import com.shelby.data.service.BookingService;
import org.junit.Test;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.view.InternalResourceView;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class ResourceControllerTest {

    @Test
    public void shouldShowResourcesList() throws Exception {
        List<Resource> expectedResources = createResourcesList(20);
        BookingService repositoryMock = mock(BookingService.class);
        when(repositoryMock.findAllResources()).thenReturn(expectedResources);

        ResourceController controller = new ResourceController(repositoryMock);
        MockMvc mockMvc = standaloneSetup(controller)
                .setSingleView(new InternalResourceView("/WEB-INF/templates/resources.html"))
                .build();

        mockMvc.perform(get("/resources"))
                .andExpect(view().name("resources"))
                .andExpect(model().attributeExists("resourceList"))
                .andExpect(model().attribute("resourceList", hasItems(expectedResources.toArray())));
    }

    @Test
    public void shouldShowResourcesList_json() throws Exception {
        List<Resource> expectedResources = createResourcesList(20);
        BookingService repositoryMock = mock(BookingService.class);
        when(repositoryMock.findAllResources()).thenReturn(expectedResources);

        ResourceController controller = new ResourceController(repositoryMock);
        MockMvc mockMvc = standaloneSetup(controller)
                .setSingleView(new InternalResourceView("/WEB-INF/templates/resources.html"))
                .build();

        mockMvc.perform(get("/resources").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(20)))
                .andExpect(jsonPath("$[0].name", is("Resource 1")))
                .andExpect(jsonPath("$[0].occupancy", is(1)))
                .andExpect(jsonPath("$[1].name", is("Resource 2")))
                .andExpect(jsonPath("$[1].occupancy", is(2)));
    }

    @Test
    public void shouldShowResourceById() throws Exception {
        Resource expectedResource = new Resource("Resource", 10);
        BookingService repositoryMock = mock(BookingService.class);
        when(repositoryMock.findResourceById(123L)).thenReturn(expectedResource);

        ResourceController controller = new ResourceController(repositoryMock);
        MockMvc mockMvc = standaloneSetup(controller)
                .setSingleView(new InternalResourceView("/WEB-INF/templates/resources.html"))
                .build();

        mockMvc.perform(get("/resources/123"))
                .andExpect(view().name("resource"))
                .andExpect(model().attributeExists("resource"))
                .andExpect(model().attribute("resource", expectedResource));
    }

    @Test
    public void shouldShowResourceById_resourceNotFound() throws Exception {
        BookingService repositoryMock = mock(BookingService.class);
        when(repositoryMock.findResourceById(123L)).thenReturn(null);

        ResourceController controller = new ResourceController(repositoryMock);
        MockMvc mockMvc = standaloneSetup(controller)
                .setSingleView(new InternalResourceView("/WEB-INF/templates/resources.html"))
                .build();

        mockMvc.perform(get("/resources/123"))
                .andExpect(status().isNotFound());
    }

    private List<Resource> createResourcesList(int count) {
        List<Resource> resources = new ArrayList<>();
        for (int i=1; i<=count; i++) {
            resources.add(new Resource("Resource " + i, i));
        }
        return resources;
    }
}
