package com.shelby.web;

import com.shelby.data.domain.Appointment;
import com.shelby.data.domain.Person;
import com.shelby.data.domain.Resource;
import com.shelby.data.service.BookingService;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.view.InternalResourceView;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class AppointmentControllerTest {

    @Test
    public void shouldShowAppointmentsList() throws Exception {
        List<Appointment> expectedAppointments = createAppointmentsList(20);
        BookingService repositoryMock = mock(BookingService.class);
        when(repositoryMock.findAllAppointments()).thenReturn(expectedAppointments);

        AppointmentController controller = new AppointmentController(repositoryMock);
        MockMvc mockMvc = standaloneSetup(controller)
                .setSingleView(new InternalResourceView("/WEB-INF/templates/appointments.html"))
                .build();

        mockMvc.perform(get("/appointments"))
                .andExpect(view().name("appointments"))
                .andExpect(model().attributeExists("appointmentList"))
                .andExpect(model().attribute("appointmentList", hasItems(expectedAppointments.toArray())));
    }

    @Test
    public void shouldShowAppointmentById() throws Exception {
        Resource resource = new Resource("Resource", 10);
        Person person = new Person(1L, "jsmith", "12345", "John", "Smith");
        Appointment expectedAppointment = new Appointment("Appointment", resource, person, LocalDateTime.now(), LocalTime.of(1, 0));
        BookingService repositoryMock = mock(BookingService.class);
        when(repositoryMock.findAppointmentById(123L)).thenReturn(expectedAppointment);

        AppointmentController controller = new AppointmentController(repositoryMock);
        MockMvc mockMvc = standaloneSetup(controller)
                .setSingleView(new InternalResourceView("/WEB-INF/templates/appointments.html"))
                .build();

        mockMvc.perform(get("/appointments/123"))
                .andExpect(view().name("appointment"))
                .andExpect(model().attributeExists("appointment"))
                .andExpect(model().attribute("appointment", expectedAppointment));
    }

    @Test
    public void shouldShowAppointmentById_appointmentNotFound() throws Exception {
        BookingService repositoryMock = mock(BookingService.class);
        when(repositoryMock.findAppointmentById(123L)).thenReturn(null);

        AppointmentController controller = new AppointmentController(repositoryMock);
        MockMvc mockMvc = standaloneSetup(controller)
                .setSingleView(new InternalResourceView("/WEB-INF/templates/appointments.html"))
                .build();

        mockMvc.perform(get("/appointments/123"))
                .andExpect(status().isNotFound());
    }

    private List<Appointment> createAppointmentsList(int count) {
        List<Appointment> appointments = new ArrayList<>();
        Resource resource = new Resource("Resource", 10);
        Person person = new Person(1L, "jsmith", "12345", "John", "Smith");
        for (int i=1; i<=count; i++) {
            appointments.add(new Appointment("Appointment " + i, resource, person, LocalDateTime.now(), LocalTime.of(1, 0)));
        }
        return appointments;
    }
}
