package com.shelby.data.service;

import com.shelby.data.domain.Appointment;
import com.shelby.data.domain.Resource;
import com.shelby.data.domain.Person;

import java.util.List;

public interface BookingService {

    List<Appointment> findAllAppointments();

    Appointment findAppointmentById(Long appointmentId);

    Appointment saveAppointment(Appointment appointment);

    List<Resource> findAllResources();

    Resource findResourceById(Long resourceId);

    Person findPersonByUsername(String username);

    Person savePerson(Person person);
}
