package com.shelby.data.service.impl;

import com.shelby.data.domain.Appointment;
import com.shelby.data.domain.Resource;
import com.shelby.data.domain.Person;
import com.shelby.data.repo.AppointmentRepository;
import com.shelby.data.repo.PersonRepository;
import com.shelby.data.repo.ResourceRepository;
import com.shelby.data.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class BookingServiceImpl implements BookingService {

    private final AppointmentRepository appointmentRepository;
    private final ResourceRepository resourceRepository;
    private final PersonRepository personRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public BookingServiceImpl(AppointmentRepository appointmentRepository, ResourceRepository resourceRepository,
                              PersonRepository personRepository, PasswordEncoder passwordEncoder) {
        this.appointmentRepository = appointmentRepository;
        this.resourceRepository = resourceRepository;
        this.personRepository = personRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public List<Appointment> findAllAppointments() {
        return appointmentRepository.findAll();
    }

    @Override
    public Appointment findAppointmentById(Long appointmentId) {
        Optional<Appointment> appointment = appointmentRepository.findById(appointmentId);
        return appointment.orElse(null);
    }

    @Override
    public Appointment saveAppointment(Appointment appointment) {
        return appointmentRepository.save(appointment);
    }

    @Override
    public List<Resource> findAllResources() {
        return resourceRepository.findAll();
    }

    @Override
    public Resource findResourceById(Long resourceId) {
        Optional<Resource> resource = resourceRepository.findById(resourceId);
        return resource.orElse(null);
    }

    @Override
    public Person findPersonByUsername(String username) {
        return personRepository.findByUsername(username);
    }

    @Override
    public Person savePerson(Person person) {
        person.setPassword(passwordEncoder.encode(person.getPassword()));
        return personRepository.save(person);
    }
}
