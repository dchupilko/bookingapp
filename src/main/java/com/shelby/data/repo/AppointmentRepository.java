package com.shelby.data.repo;

import com.shelby.data.domain.Appointment;
import com.shelby.data.domain.Resource;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppointmentRepository extends JpaRepository<Appointment, Long> {

}
