package com.shelby.web;

import com.shelby.data.domain.Resource;
import com.shelby.data.service.BookingService;
import com.shelby.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/resources")
public class ResourceController {

    private BookingService bookingService;

    @Autowired
    public ResourceController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @GetMapping
    public String resources(Model model) {
        model.addAttribute(bookingService.findAllResources());
        return "resources";
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Resource> resources() {
        return bookingService.findAllResources();
    }

    @GetMapping(value = "/{resourceId}")
    public String resource(@PathVariable long resourceId, Model model) {
        Resource resource = bookingService.findResourceById(resourceId);
        if (resource == null) {
            throw new ResourceNotFoundException(resourceId);
        }
        model.addAttribute(resource);
        return "resource";
    }

    @GetMapping(value = "/{resourceId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Resource resource(@PathVariable long resourceId) {
        Resource resource = bookingService.findResourceById(resourceId);
        if (resource == null) {
            throw new ResourceNotFoundException(resourceId);
        }
        return resource;
    }
}
