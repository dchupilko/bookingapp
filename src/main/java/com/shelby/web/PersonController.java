package com.shelby.web;

import com.shelby.data.domain.Person;
import com.shelby.data.service.BookingService;
import com.shelby.exception.PersonNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping(value = "/profile")
public class PersonController {

    private BookingService bookingService;

    @Autowired
    public PersonController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @GetMapping(value = "/new")
    public String showRegistrationForm(Model model) {
        model.addAttribute(new Person());
        return "register";
    }

    @PostMapping(value = "/new")
    public String processRegistration(@Valid Person person, Errors errors) {
        if (errors.hasErrors()) {
            return "register";
        }

        bookingService.savePerson(person);
        return "redirect:/profile/" + person.getUsername();
    }

    @GetMapping(value = "/{username}")
    public String showProfile(@PathVariable(value = "username") String username, Model model) {
        Person person = bookingService.findPersonByUsername(username);
        if (person == null) {
            throw new PersonNotFoundException(username);
        }
        model.addAttribute(person);
        return "profile";
    }

    @GetMapping(value = "/me")
    public String showProfile(Principal principal, Model model) {
        return principal == null ? "/" : showProfile(principal.getName(), model);
    }
}
