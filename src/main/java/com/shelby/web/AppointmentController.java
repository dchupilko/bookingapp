package com.shelby.web;

import com.shelby.data.domain.Appointment;
import com.shelby.data.domain.Person;
import com.shelby.data.domain.Resource;
import com.shelby.data.service.BookingService;
import com.shelby.exception.AppointmentNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Controller
@RequestMapping(value = "/appointments")
public class AppointmentController {

    private BookingService bookingService;

    @Autowired
    public AppointmentController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @GetMapping
    public String appointments(Model model) {
        model.addAttribute(bookingService.findAllAppointments());
        return "appointments";
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Appointment> appointments() {
        return bookingService.findAllAppointments();
    }

    @GetMapping(value = "/{appointmentId}")
    public String appointment(@PathVariable long appointmentId, Model model) {
        Appointment appointment = bookingService.findAppointmentById(appointmentId);
        if (appointment == null) {
            throw new AppointmentNotFoundException(appointmentId);
        }
        model.addAttribute(appointment);
        return "appointment";
    }

    @GetMapping(value = "/{appointmentId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Appointment appointment(@PathVariable long appointmentId) {
        Appointment appointment = bookingService.findAppointmentById(appointmentId);
        if (appointment == null) {
            throw new AppointmentNotFoundException(appointmentId);
        }
        return appointment;
    }

    @GetMapping(value = "/new")
    public String showRegistrationForm(Model model, Principal principal) {
        Appointment appointment = new Appointment();
        Person host = bookingService.findPersonByUsername(principal.getName());
        appointment.setHost(host);
        appointment.setStartTime(LocalDateTime.now());
        appointment.setDuration(LocalTime.of(1, 0));
        model.addAttribute(appointment);
        return "createAppointment";
    }

    @PostMapping(value = "/new")
    public String processRegistration(@Valid Appointment appointment, Errors errors) {
        if (errors.hasErrors()) {
            return "createAppointment";
        }

        bookingService.saveAppointment(appointment);
        return "redirect:/appointments/" + appointment.getId();
    }

    @ModelAttribute("allResources")
    public List<Resource> populateResources() {
        return bookingService.findAllResources();
    }
}
