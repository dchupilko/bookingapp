package com.shelby.formatter;

import com.shelby.data.domain.Resource;
import com.shelby.data.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.util.Locale;

public class ResourceFormatter implements Formatter<Resource> {

    @Autowired
    private BookingService bookingService;

    @Override
    public Resource parse(String s, Locale locale) {
        Long resourceId = Long.valueOf(s);
        return bookingService.findResourceById(resourceId);
    }

    @Override
    public String print(Resource resource, Locale locale) {
        return resource.getName() + " " + resource.getOccupancy();
    }
}
