package com.shelby.formatter;

import com.shelby.data.domain.Person;
import com.shelby.data.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.util.Locale;

public class PersonFormatter implements Formatter<Person> {

    @Autowired
    private BookingService bookingService;

    @Override
    public Person parse(String username, Locale locale) {
        return bookingService.findPersonByUsername(username);
    }

    @Override
    public String print(Person person, Locale locale) {
        return person.getUsername();
    }
}
