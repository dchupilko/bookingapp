package com.shelby.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Appointment not found")
public class AppointmentNotFoundException extends RuntimeException {

    private long appointmentId;

    public AppointmentNotFoundException(long appointmentId) {
        this.appointmentId = appointmentId;
    }

    public long getAppointmentId() {
        return appointmentId;
    }
}
