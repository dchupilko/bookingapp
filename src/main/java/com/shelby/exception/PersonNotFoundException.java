package com.shelby.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Profile not found")
public class PersonNotFoundException extends RuntimeException {

    private String username;

    public PersonNotFoundException(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}
